
PROGRAM _INIT
	request := 'AT';
	 
END_PROGRAM

PROGRAM _CYCLIC
	CASE step OF
		0:
			FRM_xopen_0.enable := TRUE;
			FRM_xopen_0.device := ADR('IF1');
			
			FRM_xopen_0();
			IF FRM_xopen_0.status = 0 THEN
				step := step + 1;
			END_IF
	
		1:
			FRM_gbuf_0.enable := TRUE;
			FRM_gbuf_0.ident := FRM_xopen_0.ident;
			FRM_gbuf_0();
			IF FRM_gbuf_0.status = 0 THEN
				step := step + 1;
			END_IF

		2:
			brsstrcpy(FRM_gbuf_0.buffer,ADR(request));
			FRM_write_0.enable := TRUE;
			FRM_write_0.ident := FRM_xopen_0.ident;
			FRM_write_0.buffer := FRM_gbuf_0.buffer;
			FRM_write_0.buflng := UDINT_TO_UINT(brsstrlen(ADR(request)));
			FRM_write_0();
			IF FRM_write_0.status = 0 THEN
				step := step + 1;
			END_IF

		3:

	END_CASE;
	
	CASE step2 OF
		0:
			IF FRM_xopen_0.ident <> 0 THEN
				step2 := step2 + 1;
			END_IF

		1:
			FRM_read_0.enable := TRUE;
			FRM_read_0.ident := FRM_xopen_0.ident;
			FRM_read_0();
			IF FRM_read_0.status = 0 THEN
				step2 := step2 + 1;
			END_IF
		
		2:
			brsmemcpy(ADR(response), FRM_read_0.buffer, FRM_read_0.buflng);
			step2 := step2 + 1;
		
		3:
		
	END_CASE;
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

