
TYPE
	MpTweetAlarmEnum : 
		( (* Alarms of MpTweet Library *)
		mpTWEET_ALM_MESSAGE_SEND_FAIL := 0, (* Failed to send message to receiver. *)
		mpTWEET_ALM_PING_TIMEOUT := 1, (* Ping command timeout with out response. *)
		mpTWEET_ALM_COMMUNICATION_FAIL := 2 (* Communication to modem failed.*)
		);
END_TYPE
